module.exports = function(grunt) {

    var ifOptionElse = function(option, alternate) {
        return grunt.option(option) || alternate;
    };

    var config = {
        pkg: grunt.file.readJSON('package.json'),
        options: {
            src: './',
            appUrl: ifOptionElse('static-url', './app/')
        }
    };
    
    'concat browserSync ngAnnotate copy sass uglify watch webpack'.split(' ').forEach(function(task){
        config[task] = require('./grunt/'+task)(grunt, config.options);
    });
    grunt.initConfig(config);


    Object.keys(require('./package.json').devDependencies).forEach(function(dep){
        if(dep.match(/grunt-/) || dep.match('assemble')) {
            grunt.loadNpmTasks(dep);
        }
    });

    grunt.registerTask('serve', ['browserSync','watch']);

};