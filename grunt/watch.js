// Watch files for changes

module.exports = function(grunt) {
	return {
		local: {
            files: ['javascripts/development/**/*.js','sass/**/*.sass','*.html'],
            tasks: ['webpack:local','sass:local','copy:js','copy:css','copy:html','copy:views'],
            options: {
                interrupt: true,
                debounceDelay: 250
            }
        }
	};
};