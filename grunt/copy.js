// Copy the files to the build/deploy directories.

module.exports = function(grunt) {
	return {
        css:{
            files: [
                {
                    expand: true,
                    src: [
                        'stylesheets/*.css'
                    ],
                    dest: '<%= options.appUrl %>'
                }
            ]
        },
        js:{
            files: [
                {
                    expand: true,
                    src: [
                        'javascripts/*.*',
                    ],
                    dest: '<%= options.appUrl %>'
                }
            ]
        },
        html: {
            files: [
                {
                    expand: true,
                    src: [
                        '*.html'
                    ],
                    dest: '<%= options.appUrl %>'
                }
            ]
        },
        views: {
            files: [
                {
                    expand: true,
                    flatten: true,
                    filter: 'isFile',
                    src: [
                        'javascripts/development/views/*.html'
                    ],
                    dest: '<%= options.appUrl %>/javascripts/views'
                }
            ]
        }
	};
};