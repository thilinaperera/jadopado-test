// Replace call to non-minified js in deploy

module.exports = function(grunt) {
	return {
        deploy: {
			src: ['<%= options.deploy %>/**/*.php'],
			overwrite: true,
			replacements: [
				{
					from: '#VERSION',
					to: '<%= pkg.cache %>'
				},
				{
					from: "'development_scripts' => 'development'",
					to: ''
				},
				{
					from: "development_scripts",
					to: 'production'
				},
				{
					from: "'angular' => 'ng-app',",
					to: ''
				},
				{
					from: "'plugins' => 'jquery.plugins.min',",
					to: ''
				},
				{
					from: "'libs' => 'jquery.libs.min',",
					to: "'production' => 'production.min',"
				},
				{
					from: "'fbID' => 1807013496184595",
					to: function(){
						if( grunt.option('iat') ){
							return "'fbID' => '1807013496184595'"
						}
						if( grunt.option('uat') ){
							return "'fbID' => '259768031060110'"
						}
						if( grunt.option('production') ){
							return "'fbID' => '1706485622907090'"
							//return "'fbID' => '1146511605392253'"
						}
					}
				}
			]
		},
        local: {
		    src: ['<%= options.local %>/**/*.php'],
		    overwrite: true,
            replacements: [
                {
					from: '#VERSION',
					to: '<%= pkg.cache %>'
                },
				{
					from: "smtp.sendgrid.net",
					to: "localhost"
				},
				{
					from: '587',
					to: '1025'
				},
				{
					from: 'azure_8863bf43b47cb45653a5753c5d5916f4@azure.com',
					to: ''
				},
				{
					from: 'dd8zeRLUhWzv',
					to: ''
				},
            ]
		}
	};
};
