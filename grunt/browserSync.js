// start file-servers for your files
// https://github.com/gruntjs/grunt-contrib-connect
module.exports = function( grunt ){
    return {
        dev: {
            bsFiles: {
                src : [
                    'app/stylesheets/*.css',
                    'app/javascripts/*.js',
                    'app/*.html'
                ]
            },
            options: {
                watchTask: true,
                server: './app'
            }
        }
    };
};
