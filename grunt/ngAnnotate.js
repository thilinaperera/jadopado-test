// Watch files for changes

module.exports = function(grunt) {
    return {
        options: {
            singleQuotes: true
        },
        app : {

            files : {
                'javascripts/safe/ng-app.js' : ['javascripts/angular/app.js','javascripts/angular/modules/*.js','javascripts/angular/controllers/*.js','javascripts/angular/directives/*.js','javascripts/angular/services/*.js', 'javascripts/angular/filters/*.js'],
            }
        }
    };
};