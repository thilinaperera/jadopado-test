// Watch files for changes

module.exports = function(grunt) {
    return {
        options: {
            // Task-specific options go here.
        },
        all: {
            src: ['styles/select2.css', 'styles/select2-bootstrap.css','styles/owl.carousel.css','styles/owl.theme.css','styles/font-awesome.min.css'],
            dest: "styles/styles.min.css"
        }
    };
};