// Minify production.js

module.exports = function(grunt) {
	return {
        lib : {
            files: {
                'javascripts/jquery.plugins.min.js': 'javascripts/plugins/*.js',
                'javascripts/jquery.libs.min.js': ['javascripts/lib/modernizr.js','javascripts/lib/angular.min.js','javascripts/lib/underscore-min.js','javascripts/lib/matchMedia.js','javascripts/lib/matchMedia.addListener.js', 'javascripts/lib/enquire.min.js','javascripts/lib/bootstrap.min.js'],
            }
        },
        deploy : {
            files : {
                'javascripts/production.min.js': ['javascripts/jquery.libs.min.js','javascripts/jquery.plugins.min.js','javascripts/development.js','javascripts/ng-app.js']
            }
        }
	};
};