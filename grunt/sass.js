// Compile Sass files

module.exports = function(grunt) {
	return {
        options: {
        	quiet: false
        },
        local: {
            options: {
                style: 'expanded'
            },
            files: {
                './stylesheets/style.css' : './sass/style.sass',
            }
        }
	};
};