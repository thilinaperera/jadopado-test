// Watch files for changes

module.exports = function(grunt) {
	return {
		dist: {
            src: [
                'javascripts/dev/*.js'
            ],
            dest: 'javascripts/development.js'
        },
        angular : {
            src: ['javascripts/safe/ng-app.js'],
            dest : 'javascripts/ng-app.js'
        }
	};
};