var webpack = require('webpack');
var ngAnnotatePlugin = require('ng-annotate-webpack-plugin');
module.exports = function(grunt) {
	return {
        local: {
            entry: {
                app :  "./javascripts/development/app.js",
                vendor : ['jquery','underscore']
            },
            output: {
                path: "./javascripts/",
                filename: "app.bundle.js"
            },
            resolve: {
                modulesDirectories: [
                    './javascripts/plugins',
                    './javascripts/development/controllers',
                    './javascripts/development/directives',
                    './javascripts/development/modules',
                    './javascripts/development/services',
                    'node_modules',
                ],
                alias: {
                    'jquery': require.resolve("jquery"),
                }
            },
            module: {
                loaders: [
                    {
                        test: /.js?$/,
                        loader: 'babel-loader',
                        exclude: /node_modules/,
                        query: {
                            presets: ['es2015']
                        }
                    }
                ]
            },
            devtool: 'source-map',
            plugins: [
                new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js"),
                new webpack.ProvidePlugin({
                    "_": "underscore"
                }),
                new webpack.ProvidePlugin({
                    $: "jquery",
                    jQuery: "jquery",
                    "window.jQuery": "jquery",
                    'window.$': 'jquery',
                }),
                new ngAnnotatePlugin({
                    add: true,
                    // other ng-annotate options here
                }),
                new webpack.optimize.UglifyJsPlugin({
                    compress: { warnings: false }
                })
            ],
        }
	};
};