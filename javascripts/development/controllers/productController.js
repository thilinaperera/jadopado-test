/**
 * Created by thilina.perera on 12/14/2016.
 */
'use strict';
productController.$inject = ['productFactory','$stateParams','$scope','$rootScope'];
export default function productController(productFactory,$stateParams,$scope,$rootScope){

    console.log('%c Controller:product', 'background: blue; color: white; display: block;');
    $scope.product = productFactory.get($stateParams.id);
    $rootScope.$watchCollection(
        "data",
        function( newValue, oldValue ) {
            if( newValue !== oldValue ){
                $scope.product = productFactory.get($stateParams.id);
            }
        }
    );
    $scope.like = ()=>{
        productFactory.like($stateParams.id);
    }

}