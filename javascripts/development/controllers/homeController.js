/**
 * Created by thilina.perera on 12/14/2016.
 */
'use strict';
homeController.$inject = ['productFactory','$scope','$rootScope'];
export default function homeController(productFactory,$scope,$rootScope){

    console.log('%c Controller:home', 'background: blue; color: white; display: block;');

    $rootScope.searchTrigger = false;
    $rootScope.currentState = 'home';

    $scope.products = productFactory.get({
        page_size : 5,
        offset: 0
    });
    $scope.offset =  $scope.products.length;

    $rootScope.$watchCollection(
        "data",
        function( newValue, oldValue ) {
            if( newValue !== oldValue ){
                $scope.products = productFactory.get();
            }
        }
    );

    $scope.like = (product_id) => {
        /**
         * check current user like
         */
        productFactory.like(product_id);
    };

    $scope.nomore = false;
    $scope.myPagingFunction = ()=>{
        if( $scope.nomore ){
            return;
        }
        let x = productFactory.get({
            page_size : 5,
            offset: $scope.offset
        });
        _.each(x,(v,k,l)=>{
            $scope.products.push(v);
        });
        if(x.length < 5 ){
            $scope.nomore = true;
        }
    }
}