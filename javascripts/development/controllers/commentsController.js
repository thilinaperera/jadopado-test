/**
 * Created by thilina.perera on 12/17/2016.
 */

commentsController.$inject = ['$rootScope','$scope','commentFactory'];
export default function commentsController($rootScope,$scope,commentFactory) {
    console.log('%c Directive:comments', 'background: blue; color: white; display: block;');
    $scope.comments = commentFactory.get($scope.product);
    $rootScope.$watchCollection(
        "data",
        function( newValue, oldValue ) {
            if( newValue !== oldValue ){
                $scope.comments = commentFactory.get($scope.product);

            }
        }
    );
    $scope.commentFormSubmit = (commentForm,$valid)=>{
        if($valid){
            commentFactory.set($scope.product,commentForm['userComment']['$modelValue']);
            $scope.userComment = "";
            setTimeout(()=>{
                document.activeElement.blur();
                window.scrollTo(0,document.body.scrollHeight);
            },300);
        }
    }
}