/**
 * Created by thilina.perera on 12/17/2016.
 */
barController.$inject = ['$rootScope','$scope'];
export default function barController($rootScope,$scope) {
    $scope.toggleSearch = ($nonSearchView = true)=> {
        //console.log('view');
        if($nonSearchView){
            $rootScope.searchTrigger = false;
            return;
        }
        $rootScope.searchTrigger = !$rootScope.searchTrigger;
    };
    $scope.activeChange = ($view,searchView = false)=>{
        $rootScope.currentState = $view;
        console.log($view);
        if($rootScope.searchTrigger === false && searchView == true){
            console.log($rootScope.toState)
            $rootScope.currentState = $rootScope.toState;
        }
    }
}