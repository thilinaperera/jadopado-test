/**
 * Created by thilina.perera on 12/14/2016.
 */
'use strict';
userFactory.$inject = ['$q','localStorageFactory'];
export default function userFactory($q,localStorage){
    let user = {
        _getUser: (id)=>{
            return localStorage.get('user',{
                id
            })
        },
        _getUsers: ()=>{

        }
    };
    return {
        get: (id = false)=>{
            if(id){
                return user._getUser(id)
            }else {
                return user._getUsers();
            }
        },
        set: ()=>{}
    }
}