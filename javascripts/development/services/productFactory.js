/**
 * Created by thilina.perera on 12/14/2016.
 */
'use strict';
productFactory.$inject = ['$q','localStorageFactory','$rootScope'];

/**
 * @ToDo upadate product -> send comments / likes separately
 * @param $q
 * @param localStorage
 * @param $stateParams
 * @returns {{get: (function(*)), set: (function())}}
 */

export default function productFactory($q,localStorage,$rootScope){
    let product = {
        _getProduct: function(id){
            return localStorage.get('product',{
                id
            });
        },
        _getProducts: function(params){
            if( typeof params != 'object'){
                // get all
                return localStorage.get('products',{ page_size : -1, offset: -1 });
            }else {
                return localStorage.get('products',{
                    page_size : params.page_size,
                    offset : params.offset
                });
            }
        },
    };
    return {
        get: (param)=>{
            if( typeof param == 'undefined'){
                // get products with defaults limits
                console.log('%c get all products with default limits', 'background: yellow; color: black; display: block;');
                return product._getProducts();
            }
            if( typeof param == "object"){
                return product._getProducts(param)
            }
            return product._getProduct(param);
        },
        set: (type,params)=>{
            if( type == 'undefined'){
                // full obj update
                return true;
            }
            switch (type){
            }

        },
        like: (product_id,user_id = $rootScope.user)=>{
            localStorage.set('like',{
                product_id,
                user_id
            });
        }
    }
}