/**
 * Created by thilina.perera on 12/14/2016.
 */
'use strict';
commentFactory.$inject = ['$q','localStorageFactory','$rootScope'];
export default function commentFactory($q,localStorage,$rootScope){
    let comment = {
        _getComments: (product_id)=>{
            return localStorage.get('comments',{
                product_id
            });
        },
        _setComments : (product_id,user_comment,user_id = $rootScope.user)=>{
            return localStorage.set('comments',{
                product_id,
                user_id,
                user_comment,
            });
        }
    };
    return {
        get: (product_id)=>{
            return comment._getComments(product_id);
        },
        set: (product_id,user_comment)=>{
            comment._setComments(product_id,user_comment)
        }
    }
}