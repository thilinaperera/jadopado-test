/**
 * Created by thilina.perera on 12/14/2016.
 */
'use strict';
import angular from 'angular';

localStorageFactory.$inject = ['$q','$rootScope'];
export default function localStorageFactory($q,$rootScope){
    let _request = {
        _data : false,
        _get: function(){

            if( $rootScope.data ){
                console.log('%c serve data from app memory', 'background: green; color: white; display: block;');
                return $rootScope.data;
            }else {
                let data = localStorage.getItem('data');
                if( !_.isNull(data) ){
                    $rootScope.data = JSON.parse(data);
                    console.log('%c local storage data found', 'background: green; color: white; display: block;');
                }else {
                    /**
                     * No data found
                     * set default data
                     */
                    $rootScope.data = {
                        users : [
                            {
                                id : 1,
                                username: 'thiper7',
                                geo_lat: -27,
                                geo_lon: 75,
                                profile_pic: 'thiper7_pro.jpg'
                            },
                            {
                                id : 2,
                                username: 'realmau5',
                                geo_lat: -27,
                                geo_lon: 75,
                                profile_pic: 'realmau5_pro.jpg'
                            },
                        ],
                        products: [
                            {
                                id: 1,
                                user_id: 1,
                                title: '1:Leaf iPhone Case Hard Plastic',
                                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat ex at porta porta. Maecenas non dolor diam. Etiam iaculis mollis ipsum eget consequat. #Suspendisse #accumsan #ugue #eu #ipsum',
                                price: 230,
                                currency_code: 'AED',
                                images: [
                                    'product_1.jpg',
                                    'product_1_1.jpg',
                                ]
                            },
                            {
                                id: 2,
                                user_id: 1,
                                title: '2:Leaf iPhone Case Soft Plastic',
                                description: 'Ut placerat ex at porta porta Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non dolor diam. Etiam iaculis mollis ipsum eget consequat. #Suspendisse #accumsan #ugue',
                                price: 530,
                                currency_code: 'AED',
                                images: [
                                    'product_2.jpg',
                                    'product_2_1.jpg',
                                ]
                            },
                            {
                                id: 3,
                                user_id: 2,
                                title: '3:Leaf iPhone Case Soft Plastic',
                                description: 'Ut placerat ex at porta porta Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non dolor diam. Etiam iaculis mollis ipsum eget consequat. #Suspendisse #accumsan #ugue',
                                price: 530,
                                currency_code: 'AED',
                                images: [
                                    'product_3.jpg',
                                ]
                            },
                            {
                                id: 4,
                                user_id: 2,
                                title: '4:Leaf iPhone Case Soft Plastic',
                                description: 'Ut placerat ex at porta porta Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non dolor diam. Etiam iaculis mollis ipsum eget consequat. #Suspendisse #accumsan #ugue',
                                price: 530,
                                currency_code: 'AED',
                                images: [
                                    'product_4.jpg',
                                ]
                            },
                            {
                                id: 5,
                                user_id: 2,
                                title: '5:Leaf iPhone Case Soft Plastic',
                                description: 'Ut placerat ex at porta porta Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non dolor diam. Etiam iaculis mollis ipsum eget consequat. #Suspendisse #accumsan #ugue',
                                price: 530,
                                currency_code: 'AED',
                                images: [
                                    'product_5.jpg',
                                ]
                            },
                            {
                                id: 6,
                                user_id: 1,
                                title: '6:Leaf iPhone Case Soft Plastic',
                                description: 'Ut placerat ex at porta porta Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non dolor diam. Etiam iaculis mollis ipsum eget consequat. #Suspendisse #accumsan #ugue',
                                price: 530,
                                currency_code: 'AED',
                                images: [
                                    'product_6.jpg',
                                ]
                            },
                            {
                                id: 7,
                                user_id: 2,
                                title: '7:Leaf iPhone Case Soft Plastic',
                                description: 'Ut placerat ex at porta porta Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non dolor diam. Etiam iaculis mollis ipsum eget consequat. #Suspendisse #accumsan #ugue',
                                price: 530,
                                currency_code: 'AED',
                                images: [
                                    'product_7.jpg',
                                ]
                            },
                            {
                                id: 8,
                                user_id: 1,
                                title: '8:Leaf iPhone Case Soft Plastic',
                                description: 'Ut placerat ex at porta porta Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non dolor diam. Etiam iaculis mollis ipsum eget consequat. #Suspendisse #accumsan #ugue',
                                price: 530,
                                currency_code: 'AED',
                                images: [
                                    'product_8.jpg',
                                    'product_8_1.jpg',
                                ]
                            }
                        ],
                        comments: [
                            {
                                id: 1,
                                user_id: 1,
                                product_id: 1,
                                comment: 'test comment 1 by u 1 Vestibulum pulvinar lectus lectus, at rutrum nibh convallis quis. Donec quis dolor hendrerit, consectetur felis id, dignissim diam. Nunc sapien quam'
                            },
                            {
                                id: 2,
                                user_id: 2,
                                product_id: 1,
                                comment: 'test comment 2 by u 2'
                            },
                            {
                                id: 3,
                                user_id: 1,
                                product_id: 2,
                                comment: 'test comment 2 by u 1 for p 2'
                            }
                        ],
                        likes: [
                            {
                                id: 1,
                                user_id: 1,
                                product_id: 1,
                            },
                            {
                                id: 2,
                                user_id: 1,
                                product_id: 2,
                            }
                        ]
                    };
                    this._set($rootScope.data);
                    console.log('%c local storage data not found', 'background: red; color: white; display: block;');
                }
            }
            return $rootScope.data;

        },
        _set: function (data = $rootScope.data || {}) {
            localStorage.setItem('data',JSON.stringify(data));
            console.log('%c write data to local storage', 'background: green; color: white; display: block;');
        },
        _getProducts: function(id = false, page_size = 5, offset = 0){
            let self = this;
            let data = this._get();
            if( id ){
                console.log('%c get product', 'background: green; color: white; display: block;');
                /**
                 * Return single product
                 * bind comments & likes
                 */
                let product = _.findWhere(data.products,{ id });
                if( typeof product == "undefined"){
                    console.log('%c invalid product id: '+id, 'background: red; color: white; display: block;');
                    return false;
                }
                /**
                 * get comments
                  */
                product.comments = _.where(data.comments,{ product_id : id }) || [];
                /**
                 * likes
                 */
                product.likes = _.where(data.likes,{ product_id : id }) || [];

                /**
                 * user data
                 */
                product.user = self._getUsers(product.user_id);

                return product;
            }

            /**
             * get all products without limits
             */
            console.log('%c get products', 'background: green; color: white; display: block;');
            let products =_.map(data.products,(product)=>{
                product.comments = _.where(data.comments,{ product_id : product.id }) || [];
                product.likes = _.where(data.likes,{ product_id : product.id }) || [];
                product.user = self._getUsers(product.user_id);
                return product;
            });

            if( page_size == -1 ){

                return products;
            }
            /**
             * limit results
             */
            let productsOffset =  _.rest(products,offset);
            console.log(products);
            console.log(offset);
            console.log(productsOffset)
            return _.initial(productsOffset,productsOffset.length - 5);

        },
        _getComments: function(product_id){
            /**
             * get all comments
             */
            let self = this;
            console.log('%c get comments', 'background: green; color: white; display: block;');
            let data = this._get();

            let comments = _.where(data.comments,{ product_id }) || [];
            let commentsWithUsers = _.map(comments,(comment)=>{
                comment.user = self._getUsers(comment.user_id)
                return comment;
            });
            return commentsWithUsers;
        },
        _getUsers: function(id){
            let data = this._get();
            let user = _.findWhere(data.users,{ id });
            return user;
        },
        _addComment: function( product_id,user_id,comment ){
            console.log('%c add comments to: '+product_id, 'background: green; color: white; display: block;');
            let _temp = {};
            angular.copy(this._get(),_temp);
            //console.log(_temp);
            _temp.comments.push({
                id : +new Date(),
                product_id,
                user_id,
                comment
            });
            $rootScope.data = _temp;
        },
        _updateLike: function(product_id,user_id){

            let _temp = {};
            angular.copy(this._get(),_temp);

            let like_index = _.findIndex(_temp.likes,{ product_id, user_id });
            if( like_index !== -1 ){
                /**
                 * remove like
                 */
                console.log('%c remove like from: '+product_id, 'background: red; color: white; display: block;');
                _temp.likes.splice(like_index,1);


            }else {
                /**
                 * add like
                 */
                console.log('%c add like to: '+product_id, 'background: green; color: white; display: block;');
                _temp.likes.push({
                    id : +new Date(),
                    product_id,
                    user_id
                });
            }
            $rootScope.data =  _temp;
        }
    };
    return {
        get: (type = false,prams = false)=>{
            if(type){
                switch (type){
                    case 'product':
                        let _id = parseInt(prams.id);
                        if( _.isNaN(_id) ){
                            return false;
                        }
                        return _request._getProducts(_id);
                        break;
                    case 'products':
                        return _request._getProducts(false, prams.page_size, prams.offset );
                        break;
                    case 'comments':
                        let _product_id = parseInt(prams.product_id);
                        if( _.isNaN(_product_id) ){
                            return false;
                        }
                        return _request._getComments(_product_id);
                        break;
                    case 'user':
                        let _user_id = parseInt(prams.id);
                        return _request._getUsers(_user_id)
                        break;
                    case 'users':
                        break;
                }
            }
        },
        set: (type = false ,data = false)=>{
            console.log(type);
            console.log(data);
            if(type){
                switch (type){
                    case 'like':
                        let product_id = parseInt(data.product_id);
                        if( _.isNaN(product_id) ){
                            return false;
                        }
                        let user_id = parseInt(data.user_id);
                        if( _.isNaN(user_id) ){
                            return false;
                        }
                        _request._updateLike(product_id,user_id);
                        break;
                    case 'comments':
                        let c_product = parseInt(data.product_id);
                        if( _.isNaN(c_product) ){
                            return false;
                        }
                        let c_user_id = parseInt(data.user_id);
                        if( _.isNaN(c_user_id) ){
                            return false;
                        }
                        _request._addComment(c_product,c_user_id,data.user_comment);
                        break;
                }
            }
            _request._set();
        }
    }
}