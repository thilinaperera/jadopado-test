import angular from 'angular';
import uirouter from 'angular-ui-router';

var ngTouch = require('angular-touch');
var carousel  = require('angular-carousel');
import ngInfiniteScroll from 'ng-infinite-scroll';

// services & factories
import userFactory from 'userFactory'
import productFactory from 'productFactory'
import localStorageFactory from 'localStorageFactory'
import commentFactory from 'commentFactory'

// directives
import comments from 'commentsDirective'
import slider from 'sliderDirective'
import bar from 'barDirective'
import search from 'searchDirective'

// controllers
import homeController from 'homeController'
import productController from 'productController'


// app define
let app = angular.module('app',[uirouter,'angular-carousel',ngInfiniteScroll]);

// Services and factories
app.factory('userFactory',userFactory);
app.factory('productFactory',productFactory);
app.factory('commentFactory',commentFactory);
app.factory('localStorageFactory',localStorageFactory);


// directives
app.directive('comment',comments);
app.directive('slider',slider);
app.directive('bar',bar);
app.directive('search',search);


// Routes
app.config(($stateProvider,$urlRouterProvider)=>{

    $stateProvider
        .state('home',{
            url: '/',
            templateUrl : 'javascripts/views/home.html',
            controller : homeController
        })
        .state('product',{
            url : '/product/:id',
            templateUrl : 'javascripts/views/product.html',
            controller: productController
        });
    $urlRouterProvider.otherwise('/');
});
app.run(($stateParams,$rootScope)=>{
    $rootScope.user = 1;
    $rootScope.searchTrigger = false;
    $rootScope.currentState = false;
    $rootScope.toState = false;
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams, options){
            $rootScope.toState = toState.name;
        }
    )
});