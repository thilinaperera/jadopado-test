/**
 * Created by thilina.perera on 12/15/2016.
 */
import angular from 'angular';
require('ion-rangeslider');
export default function slider(){
    return {
        restrict: "AE",
        require: 'ngModel',
        scope: {
            ngModel: '='
        },
        link: (scope, element, attrs, ngModelCtrl)=>{
            console.log(scope.ngModel);
            angular.element(element).ionRangeSlider({
                min: 1,
                max: 1000,
                from: scope.ngModel,
                onChange : (data)=>{
                    scope.ngModel = data.form
                },
                hide_min_max: true,
                hide_from_to: true,
                grid: false

            });
        }
    }
}