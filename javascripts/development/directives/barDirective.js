/**
 * Created by thilina.perera on 12/15/2016.
 */
import barController from 'barController';
export default function bar(){
    return {
        replace : true,
        templateUrl : 'javascripts/views/bar.html',
        controller: barController
    }
}