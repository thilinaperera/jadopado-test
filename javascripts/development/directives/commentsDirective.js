/**
 * Created by thilina.perera on 12/15/2016.
 */
import commentsController from 'commentsController'
export default function comments(){
    return {
        scope: {
            product: '='
        },
        templateUrl : 'javascripts/views/comments.html',
        controller: commentsController
    }
}