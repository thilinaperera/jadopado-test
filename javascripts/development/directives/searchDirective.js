import searchController from 'searchController';
export default function search(){
    return {
        replace : true,
        templateUrl : 'javascripts/views/search.html',
        controller: searchController
    }
}